import React from 'react';

import Friend from './friend';

// NB! Had to do this because of how numbers are formatted sometimes.
function roundCoins(amount) {
    let num = amount.toString();
    num = num.replace(/\,/g, '');
    return parseFloat(num).toFixed(2);
}

class Profile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            profile: props.profile,
            euro: 0.00,
            pound: 0.00,
            dollar: 0.00,
            friends: [],
        }
    }

    // Keep state/props in line.
    componentWillReceiveProps(nProps) {
        this.setState({ profile: nProps.profile });
    }

    componentDidMount() {
        const that = this;
        
        // Grab some bitcoin data, just for fun.
        fetch('https://api.coindesk.com/v1/bpi/currentprice.json')
            .then(res => {
                return res.json();
            })
            .then(json => {
                this.setState({ euro: json.bpi.EUR.rate });
                this.setState({ pound: json.bpi.GBP.rate });
                this.setState({ dollar: json.bpi.USD.rate });
            })
            .catch(err => {
                alertify.error('Failed to get Bitcoin data.');
            });
        
        // Grab some friends' data.
        fetch('https://randomuser.me/api/?results=8&nat=US')
            .then(res => {
                return res.json();
            })
            .then(json => {
                that.setState({ friends: json.results });
            })
            .catch(err => {
                alertify.error('Failed to get friends data.');
            });
    }

    render() {
        let data = this.state.profile;
        let friends = [];

        this.state.friends.forEach(f => {
            friends.push(<Friend {...f} key={f.login.md5} />)
        })

        return(
            <div className="grid-x">
                <div className="profile-pic-box small-12">
                    <img id="profile-pic" src={data.picture.large} alt="Profile Pic" />
                    <span className="profile-name">
                        {data.name.first.toUpperCase()} &nbsp;
                        {data.name.last.toUpperCase()}
                    </span>
                    <br />
                    <span>
                        {data.location.city} &nbsp; | &nbsp;
                        {data.location.state}
                    </span>
                    <br />
                    <span>
                        {data.cell}
                    </span>
                </div>

                <div className="friend-list small-12">
                    <h3>Top {this.state.friends.length} Bitcoin Buddies</h3>
                    {friends}
                </div>
                
                <div className="bitcoin-data small-12">
                    <h3>Bitcoin Exchange Rates</h3>
                    <span><b>&euro;</b> {roundCoins(this.state.euro)} &nbsp; | &nbsp;</span>
                    <span><b>&pound;</b> {roundCoins(this.state.pound)} &nbsp; | &nbsp;</span>
                    <span><b>&#36;</b> {roundCoins(this.state.dollar)}</span>
                </div>
            </div>
        );
    }
}

export default Profile;