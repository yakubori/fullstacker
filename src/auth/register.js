import React from 'react';

// ----------------------------------------------------------------------------
// The Regsitration/Login component.
// ----------------------------------------------------------------------------
class Register extends React.Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        // Check if a session exists and re-route if so.
        // NB! The same-origin credential shares the cookie in the request.
        fetch('/api/session', { credentials: "same-origin" })
            .then((res) => {
                return res.json();
            })
            .then((json) => {
                // Session exists.
                if (json.status && json.status !== 200) {
                    window.location = '/#/a/home';
                }
            })
            .catch((err) => {
                console.error('session error:', err);
            });
    }

    handleLogin(e) {
        e.preventDefault();
        e.stopPropagation();

        const email = $('#login-email').val().trim();
        const password = $('#login-password').val().trim();

        fetch('/api/login', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            method: 'POST',
            redirect: 'follow',
            body: JSON.stringify({
                logemail: email,
                logpassword: password,
            }),
        })
        .then(res => {
            return res.json();
        })
        .then(json => {
            if (json.status && json.status !== 200) {
                alertify.error(json.error);
            } else {
                window.location = "/#/a/home";
            }
        })
        .catch(err => {
            alertify.error('Login failed. Please contact support.');
        });
    }

    handleLoginClick(e) {
        e.preventDefault();
        e.stopPropagation();

        $('#register-form').hide();
        $('#login-form').show();
        $('#login-email').focus();
    }

    handleRegsiter(e) {
        e.preventDefault();
        e.stopPropagation();

        const email = $('#register-email').val().trim();
        const username = $('#register-username').val().trim();
        const password = $('#register-password').val().trim();
        const passwordConf = $('#register-confirm-password').val().trim();

        fetch('/api/register', {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            credentials: 'same-origin',
            method: 'POST',
            redirect: 'follow',
            body: JSON.stringify({
                email: email,
                username: username,
                password: password,
                passwordConf: passwordConf,
            }),
        })
        .then(res => {
            return res.json();
        })
        .then(json => {
            if (json.status && json.status !== 200) {
                alertify.error(json.error);
            } else {
                window.location = "/#/a/home";
            }
        })
        .catch(err => {
            alertify.error('Failed to register. Please contact support.');
        })
    }
    
    handleRegisterClick(e) {
        e.preventDefault();
        e.stopPropagation();
        
        $('#login-form').hide();
        $('#register-form').show();
        $('#register-email').focus();
    }

    render() {
        return (
            <div className="grid-x">
                <div className="register-form-box small-12 medium-6 large-3">
                    <form id="register-form" onSubmit={this.handleRegsiter.bind(this)} className="small-12 medium-6 large-3">
                        <input type="text" id="register-email" name="email" placeholder="Email" autoFocus />
                        <input type="text" id="register-username" name="username" placeholder="Username" />
                        <input type="password" id="register-password" name="password" placeholder="Password" />
                        <input type="password" id="register-confirm-password" name="passwordConf" placeholder="Confirm Password" />

                        <input type="submit" id="register-submit" className="button primary" value="Register" />
                        <a href="#" className="register-link" id="login-link" onClick={this.handleLoginClick.bind(this)}>Login</a>
                    </form>

                    <form id="login-form" onSubmit={this.handleLogin.bind(this)} className="small-12 medium-6 large-3">
                        <input type="text" id="login-email" name="logemail" placeholder="Email" autoFocus />
                        <input type="password" id="login-password" name="logpassword" placeholder="Password" />
                        
                        <input type="submit" id="login-submit" className="button primary" value="Login" />
                        <a href="#" className="register-link" id="register-link" onClick={this.handleRegisterClick.bind(this)}>Register</a>
                    </form>
                </div>
            </div>
        );
    }
}

export default Register;