import React from 'react';

class Friend extends React.Component {
    constructor(props) {
        super(props);
    }

    handleMouseOver(e) {
        $('img.friend-pic').css('opacity', '.2');
        $(e.target).css('opacity', '1');
        $(e.target).css('width', '60px');
    }
    
    handleMouseOut(e) {
        $(e.target).css('width', '');
        $('img.friend-pic').css('opacity', '1');
    }

    render() {
        return <img title={this.props.name.first + ' ' + this.props.name.last}
            className="friend-pic"
            onMouseOver={this.handleMouseOver.bind(this)}
            onMouseOut={this.handleMouseOut.bind(this)}
            src={this.props.picture.thumbnail} alt="Friend Pic" />;
    }
}

export default Friend;