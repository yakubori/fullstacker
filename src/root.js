import React from 'react';
import { Route, Link, Redirect } from 'react-router-dom';

import Register from './auth/register';
import Home from './home';

class Root extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        // Normal workflow
        return (
            <div>
                <Route exact path="/" component={Register} />
                <Route path="/a/home" component={Home} />
            </div>
        );
    }
}

export default Root;