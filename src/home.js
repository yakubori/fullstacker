import React from 'react';

import Profile from './profile';

class Home extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            id: "",
            email: "",
            username: "",
            profile: {
                email: "",
                cell: "",
                picture: {
                    large: "",
                    medium: "",
                    small: "",
                },
                name: {
                    title: "",
                    first: "",
                    last: "",
                },
                location: {
                    street: "",
                    city: "",
                    state: "",
                    postcode: 0,
                },
            },
        }
    }

    componentWillMount() {
        const that = this;

        // Check if a session exists and re-route if so.
        // NB! The same-origin credential shares the cookie in the request.
        fetch('/api/session', { credentials: 'same-origin' })
            .then((res) => {
                return res.json();
            })
            .then((json) => {
                // Session does NOT exist.
                if (!json.status) { window.location = '/#/'; }
                else {
                    fetch('/api/profile', { credentials: 'same-origin' })
                        .then(res => {
                            return res.json();
                        })
                        .then(json => {
                            this.setState({
                                id: json['_id'],
                                email: json.email,
                                username: json.username,
                            });
                        })
                        .catch(err => {
                            console.error('Failed to get profile.', err);
                        });
                    
                    // Grab some extra profile data for free.
                    fetch('https://randomuser.me/api/?nat=US')
                        .then(res => {
                            return res.json()
                        })
                        .then(json => {
                            that.setState({ profile: json.results[0] });

                        })
                        .catch(err => {
                            console.error('Failed to get randomuser data.', err);
                        });
                }
            })
            .catch((err) => {
                console.error('session error:', err);
            });
    }

    handleLogout(e) {
        e.preventDefault();
        e.stopPropagation();

        window.location = '/api/logout';
    }

    handleDelete(e) {
        e.preventDefault();
        e.stopPropagation();
        
        alertify.confirm(
            'Delete User',
            'Are you sure you want to delete this user?',
            function () {
                fetch('/api/user', { credentials: 'same-origin', method: 'delete' })
                    .then(res => {
                        return res.json();
                    })
                    .then(json => {
                        window.location = '/';
                    });
            },
            function () {
                return;
            });
    }

    render() {
        return (
            <div className="grid-x">
                <div className="home-box small-12">
                    <button id="logout-btn" className="button primary" onClick={this.handleLogout.bind(this)}>Logout</button>
                    <button id="delete-btn" className="button alert" onClick={this.handleDelete.bind(this)}>Delete</button>
                    <h1>Welcome, {this.state.username}</h1>

                    <Profile profile={this.state.profile} />
                </div>
            </div>
        )
    }
}

export default Home;