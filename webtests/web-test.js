module.exports = {
    'Register and Logout': function (browser) {
        browser
            .url(browser.launchUrl)
            .waitForElementVisible('body', 3000)
            .waitForElementVisible('#register-form', 3000)
            .waitForElementVisible('#register-submit', 1000)
            .setValue('#register-email', 'testuser@example.com')
            .setValue('#register-username', 'testuser')
            .setValue('#register-password', 'password')
            .setValue('#register-confirm-password', 'password')
            .click('#register-submit')
            .pause(1000)
            .assert.containsText('body', 'Welcome, testuser')
            .waitForElementVisible('#logout-btn', 1000)
            .click('#logout-btn')
            .pause(1000)
            .end();
    },

    'Login and Delete User': function(browser) {
        browser
            .url(browser.launchUrl)
            .waitForElementVisible('body', 3000)
            .waitForElementVisible('#login-link', 1000)
            .click('#login-link')
            .pause(1000)
            .waitForElementVisible('#login-email', 1000)
            .waitForElementVisible('#login-password', 1000)
            .waitForElementVisible('#login-submit', 1000)
            .setValue('#login-email', 'testuser@example.com')
            .setValue('#login-password', 'password')
            .click('#login-submit')
            .pause(1000)
            .waitForElementVisible('#delete-btn', 3000)
            .click('#delete-btn')
            .waitForElementVisible('button.ajs-ok', 3000)
            .click('button.ajs-ok')
            .waitForElementVisible('#register-form', 3000)
            .end()
    }
};