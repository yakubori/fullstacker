const express = require('express');
const router = express.Router();
const User = require('../models/user');

// ----------------------------------------------------------------------------
// Middleware to check for unauthorized user.
// ----------------------------------------------------------------------------
function authCheck(req, res, next) {
    if (!req.session.userId) {
        let err = new Error('Unauthorized user.');
        err.status = 401;
        return next(err);
    }
    
    return next();
}

// ----------------------------------------------------------------------------
// Middleware to check for an existing session.
// ----------------------------------------------------------------------------
function isAuthed(req, res, next) {
    if (req.session.userId) {
        let err = new Error('User session exists.');
        err.status = 400;
        return next(err);
    }

    return next();
}

// ----------------------------------------------------------------------------
// Check if the session exists.
// ----------------------------------------------------------------------------
router.get('/api/session', isAuthed, (req, res, next) => {
    return res.json({ message: 'No session exists. You are free to authenticate.' });
});

// ----------------------------------------------------------------------------
// Handle registration.
// ----------------------------------------------------------------------------
router.post('/api/register', isAuthed, (req, res, next) => {
    // confirm that user typed same password twice
    if (req.body.password !== req.body.passwordConf) {
        let err = new Error('Passwords do not match.');
        err.status = 400;
        return next(err);
    }

    // Deconstruct req.body;
    let { email, username, password, passwordConf } = req.body;

    if (email && username && password && passwordConf) {
        let userData = {
            email: email,
            username: username,
            password: password,
            passwordConf: passwordConf,
        }
        
        User.create(userData, (error, user) => {
            let err;

            if (error) {
                switch (error.code) {
                    case 11000:
                        err = new Error('Duplicate user registration. Please use a unique email and username.');
                        err.status = 409;
                        return next(err);
                        break;
                    default:
                        return next(error);
                        break;
                }
            }

            req.session.userId = user._id;
            return res.json({ message: "Registration successful." });
        });
    } else {
        let err = new Error('All fields are required.');
        err.status = 400;
        return next(err);
    }
    
});

// ----------------------------------------------------------------------------
// Handle login.
// ----------------------------------------------------------------------------
router.post('/api/login', isAuthed, (req, res, next) => {
    if (req.body.logemail && req.body.logpassword) {
        User.authenticate(req.body.logemail, req.body.logpassword, (error, user) => {
            if (error || !user) {
                let err = new Error('Wrong email or password.');
                err.status = 401;
                return next(err);
            }
            
            req.session.userId = user._id;
            return res.json({ message: "Login successful." });
        });
    } else {
        let err = new Error('Email and password are required.');
        err.status = 400;
        return next(err);
    }
    
});

// ----------------------------------------------------------------------------
// Get user profile data.
// ----------------------------------------------------------------------------
router.get('/api/profile', authCheck, (req, res, next) => {
    User.findById(req.session.userId)
        .exec((error, user) => {
            if (error) { return next(error); }
            
            if (user === null) {
                let err = new Error('Not authorized! Go back!');
                err.status = 400;
                return next(err);
            }
            
            // Destruction of the user object.
            // We don't need password, etc.
            let rv_user = (({ _id, email, username }) => ({ _id, email, username }))(user);

            return res.json(rv_user);
        });
    });
    
// ----------------------------------------------------------------------------
// Destroy the http session.
// ----------------------------------------------------------------------------
router.get('/api/logout', (req, res, next) => {
    if (req.session) {
        // delete session object
        req.session.destroy((err) => {
            if (err) { return next(err); }
            
            return res.redirect('/');
        });
    } else {
        return res.redirect('/');
    }
});

// ----------------------------------------------------------------------------
// Delete user based on session.
// ----------------------------------------------------------------------------
router.delete('/api/user', (req, res, next) => {
    if (!req.session.userId) {
        let err = new Error('No session to terminate.');
        err.status = 400;
        return next(err);
    }

    User.findByIdAndRemove(req.session.userId, (err, doc) => {
        if (err) {
            console.error('Failed to remove user:', req.session.userId, err);
            let error = new Error('Failed to delete user.')
            return next(error);
        }
        
        // delete session object
        req.session.destroy((err) => {
            if (err) { return next(err); }

            return res.json({ message: 'User deleted.' });
        });
    });
});

module.exports = router;