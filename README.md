# Fullstacker #

A simple example of a fullstack JavaScript application, with tests!

This project represents what we can do with JavaScript, from the database layer up to the browser layer,
not to mention the automated testing! We've got *MongoDB* for the database, *Node.js* using *Express.js* for the web API layer, *React.js* for the browser layer, *Mocha/Chai* for the API tests, and *Nightwatch.js* with *Selenium* for the browser tests.

While the application itself is simple, it demonstrates the full course of development we can achieve with
a single programming language. THAT'S AWESOME!

## What does it do? ##

In the application, you can register a new user with an email address, username, and password. This will
bring you to a mock-profile view of a BitCoin trader (with the help of randomuser.me and coindesk). From
there, you can logout (and optionally login again) or delete the user. That's it! Pretty simple!

## Setup ##

Installing global dependencies.

`npm install -g browserify mocha`

Installing application dependencies.

`npm install`

Building the React app (this requires browserify).

**NB!** The app.js file should already be built and ready to run.

`npm run build`

## Run the Services ##

Start MongoDB.

`service mongodb start`

Start the express app.

`npm start`

## Tests ##

**NB!** Make sure the express app is running before you start the tests!

Run the API tests (this requires mocha).

`npm test`

Run the browser tests (this requires nightwatch and selenium server, which are included).

**NB!** The testweb suite is run with Chrome.

`npm run testweb`

## Notes ##

An internet connection is required for the client side libraries hosted on Cloudflare CDN.

 - jQuery
 - Foundation
 - Alertify

It is also required for the user profile and bitcoin data from:

 - randomuser.me
 - coindesk