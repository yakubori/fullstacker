const express = require('express');
const bodyParser = require('body-parser');
const session = require('express-session');

const mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(session);

const app = express();
const PORT = 3000;

// ----------------------------------------------------------------------------
// Connect to the database and setup sessions and routes.
// ----------------------------------------------------------------------------
mongoose.connect('mongodb://localhost/fullstacker')
    .then(() => {
        let db = mongoose.connection;
        console.log('database connection succeeded...');
        return db;
    })
    .then((db) => {
        //use sessions for tracking logins
        app.use(session({
            secret: 'Billy likes soda.',
            resave: true,
            saveUninitialized: false,
            store: new MongoStore({ mongooseConnection: db }),
        }));

        // parse incoming requests
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: false }));

        // serve static files from 'public'.
        // NB! This will give us index.html automatically.
        app.use(express.static(__dirname + '/public'));

        // include routes
        let routes = require('./routes/router');
        app.use('/', routes);

        // catch 404 and forward to error handler
        app.use((req, res, next) => {
            let err = new Error('File Not Found');
            err.status = 404;
            next(err);
        });

        // error handler
        app.use((err, req, res, next) => {
            let status = err.status || 500;
            
            res.status(status);
            res.json({ status: status, error: err.message });
        });

        app.listen(PORT, () => {
            console.log(`Fullstacker app listening on port ${PORT}...`);
        });
    })
    .catch((err) => {
        console.error('Failed to connect to database:', err.message);
    });