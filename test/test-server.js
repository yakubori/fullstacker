const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();

const server = "http://localhost:3000";

chai.use(chaiHttp);

describe('API Tests', () => {
    it('should NOT allow a bad registration...', (done) => {
        chai.request(server)
            .post('/api/register')
            .end((err, res) => {
                if (err) {
                    console.error('Make sure the server is started!');
                    done(err);
                }

                res.should.have.status(400);
                res.should.have.property('body');
                res.body.should.have.property('status');
                res.body.should.have.property('error');
                res.body.status.should.equal(400);
                res.body.error.should.equal('All fields are required.');
                done();
            });
    });

    it('should NOT allow a bad login...', (done) => {
        chai.request(server)
            .post('/api/login')
            .end((err, res) => {
                if (err) {
                    console.error('Make sure the server is started!');
                    done(err);
                }

                res.should.have.status(400);
                res.should.have.property('body');
                res.body.should.have.property('status');
                res.body.should.have.property('error');
                res.body.status.should.equal(400);
                res.body.error.should.equal('Email and password are required.');
                done();
            });
    });

    it('should NOT allow a delete-user without a session...', (done) => {
        chai.request(server)
            .del('/api/user')
            .end((err, res) => {
                if (err) {
                    console.error('Make sure the server is started!');
                    done(err);
                }

                res.should.have.status(400);
                res.should.have.property('body');
                res.body.should.have.property('status');
                res.body.should.have.property('error');
                res.body.status.should.equal(400);
                res.body.error.should.equal('No session to terminate.');
                done();
            });
    });

    it('should show no session exists...', (done) => {
        chai.request(server)
            .get('/api/session')
            .end((err, res) => {
                if (err) {
                    console.error('Make sure the server is started!');
                    done(err);
                }

                res.should.have.status(200);
                res.body.should.have.property('message');
                res.body.message.should.equal('No session exists. You are free to authenticate.');
                done();
            });
    });

    it('should allow a registration...', (done) => {
        chai.request(server)
            .post('/api/register')
            .send({
                email: "testuser@example.com",
                username: "testuser",
                password: "password",
                passwordConf: "password",
            })
            .end((err, res) => {
                if (err) {
                    console.error('Make sure the server is started!');
                    done(err);
                }

                res.should.have.status(200);
                res.should.have.property('body');
                res.body.should.have.property('message');
                res.body.message.should.equal('Registration successful.');
                done();
            });
    });

    it('should NOT allow a duplicate registration...', (done) => {
        chai.request(server)
            .post('/api/register')
            .send({
                email: "testuser@example.com",
                username: "testuser",
                password: "password",
                passwordConf: "password",
            })
            .end((err, res) => {
                if (err) {
                    console.error('Make sure the server is started!');
                    done(err);
                }

                res.should.have.status(409);
                res.should.have.property('body');
                res.body.should.have.property('error');
                res.body.error.should.equal('Duplicate user registration. Please use a unique email and username.');
                done();
            });
    });

    it('should allow a user to logout...', (done) => {
        chai.request(server)
            .get('/api/logout')
            .end((err, res) => {
                if (err) {
                    console.error('Make sure the server is started!');
                    done(err);
                }

                res.should.have.status(200);
                res.should.have.property('redirects');
                res.redirects[0].should.equal(`${server}/`);
                done();
            });
    });

    it('should allow a user to login...', (done) => {
        chai.request(server)
            .post('/api/login')
            .send({
                logemail: "testuser@example.com",
                logpassword: "password",
            })
            .end((err, res) => {
                if (err) {
                    console.error('Make sure the server is started!');
                    done(err);
                }

                res.should.have.status(200);
                res.should.have.property('body');
                res.body.should.have.property('message');
                res.body.message.should.equal('Login successful.');
                done();
            })
    });

    it('should allow a user to delete themselves...', (done) => {
        let agent = chai.request.agent(server)

        // NB! This test requires a session, so we login first.
        agent.post('/api/login')
            .send({
                logemail: "testuser@example.com",
                logpassword: "password",
            })
            .end((err, res) => {
                if (err) {
                    console.error('Make sure the server is started!');
                    done(err);
                }

                res.should.have.status(200);
                res.should.have.property('body');
                res.body.should.have.property('message');
                res.body.message.should.equal('Login successful.');

                agent.del('/api/user')
                    .then((res) => {
                        res.should.have.status(200);
                        res.should.have.property('body');
                        res.body.should.have.property('message');
                        res.body.message.should.equal('User deleted.');
                        done();
                    })
                    .catch(err => {
                        console.error('FAIL:', err);
                        done(err);
                    });
            });
    });
});